# Front-end Engineer Evaluation

<img src="https://gitlab.com/treatspace/developer/front-end-engineer-evaluation/raw/master/assets/img/FinishedDesign.png" />


This project is designed to be a simple Front-end Engineer evaluation to allow us to get a first hand look at how job candidates think and approach a problem. As a company, we feel like whiteboarding interviews don't truly test an engineer on their abilities that they use on a day to day basis, but rather test their ability to solve HackerRank style questions. 

Our goal during our interview process is to have a conversation with you to see how you think and approach problems, learn your interests, and identify your strong suits. We don't think interviews should be an interrogation or a test, therefore, we don't use automated coding tests or whiteboard sessions during our process. 

Instead, we put together this simple evaluation process that will have you do a few small things that do happen on a very regular basis here at Treatspace and in most front-end engineering jobs. Below you will find the simple installation process as well as details as to what we would like you to do. 

## Installing / Getting Started

A quick introduction of the minimial setup you need to get this application up and running. 

Fork a copy of the project into your own repo, then:

```shell
git clone https://gitlab.com/treatspace/developer/front-end-engineer-evaluation.git
cd front-end-engineer-evaluation
npm install
```

Once you have all of the packages installed, remain in the root directory of the project and run `gulp`
This will start the processes in the gulpfile to compile, minify, and compress our SCSS/JS files. From there all you need to do is open index.html and you are free to begin coding!

## Developing
The idea behind this project is to demonstrate 2 very basic skills that you would encounter on a daily basis here at Treatspace:

1. Being able to create a page with a certain style based off of screenshots

2. Being able to pull data from a GET request and update the frontend based on the data you receive.

For this project, I would like you to use the OpenDataPA API here (https://data.pa.gov/Covid-19/COVID-19-Vaccinations-by-Residence-Current-County-/gcnb-epac) and pull in the current covid vaccination information as seen in the screenshot.
We should be able to sort any column as well as have pagination displaying 10 rows at a time at a maximum. In the table headings, you can use this icon for the sorting icon (<i class="fas fa-sort"></i>)


### OpenDataPA API
You may need to register for a free app token as referenced in the documentation to be used in your local testing, but when submitting your code, feel free to remove that as we will use our own test key when running your code. 


### Things we'll be looking at
Once you submit you merge request, these are some of the things we will be taking a look at to see how you approached the problem:

* Does the UI match the screenshots
* Does the UI scale well across multiple screen sizes? (We have provided a desktop sized design, but how it translates to mobile screens is up to you)
* How did you approach the API & update the UI with the data?

We understand that many people already have full time jobs as well as a home life, so we only want you to spend 3-4 hours MAX working on this problem. If you can finish it quicker than that, that is perfectly fine, but don't worry about spending long hours working on this. This evaluation is simply to see how you approach a problem and allow us to have a conversation around your approach.



## When you are finished
Once you've completed your code, email Chad (chad.moyer@treatspace.com) and Noah (noah.scholfield@treatspace.com) with a link to your repo to fork and review your code. If you have any questions regarding the project or if something was unclear, feel free to reach out and we'll get back to you as soon as we can! 

We look forward to talking with you!





